CARGO := cargo
CBUILD := $(CARGO) build --release

all: build

build:
	$(CBUILD)

lin64:
	$(CBUILD) --target x86_64-unknown-linux-gnu

win64:
	$(CBUILD) --target x86_64-pc-windows-gnu

clean:
	$(CARGO) clean

.PHONY: all
