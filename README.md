# Register Machine

## Instructions

- **PRINT**: Print the current accumulator and registry values
- **LOAD**: Load a value into accumulator
- **STORE**: Save the accumulator into registry index i (i cannot be const)
- **ADD**: Add value to accumulator
- **SUB**: Subtract value from accumulator
- **MUL**: Multiply accumulator with value
- **DIV**: Divide accumulator by value
- **GOTO**: TODO
- **JZERO**: TODO
- **JNZERO**: TODO
- **END**: Terminate the process
