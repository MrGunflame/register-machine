use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::PathBuf;
use std::process;
use std::result;

enum CompileError {
    InvalidInstruction,
    InvalidOperand,
}

// fn compile_error(err: CompileError, ln: usize, obj: &str, ctx: &str) -> ! {
//     let msg = match err {
//         CompileError::InvalidInstruction => format!("cannot find instruction `{}`", obj),
//         CompileError::InvalidOperand => format!("invalid operand `{}`", obj),
//     };

//     process::exit(1);
// }

enum OperandType {
    Const,
    Ref,
    Ptr,
}

fn operand_type(s: &str) -> OperandType {
    match &s[..1] {
        "#" => OperandType::Const,
        "*" => OperandType::Ptr,
        _ => OperandType::Ref,
    }
}

struct RegisterMachine<'a> {
    acc: u8,
    reg: [u8; 32],
    instructions: &'a Vec<Instruction>,
    cur: usize,
    flags: &'a HashMap<String, usize>,
    end: bool,
}

impl<'a> RegisterMachine<'a> {
    pub fn new(
        instructions: &'a Vec<Instruction>,
        map: &'a HashMap<String, usize>,
    ) -> RegisterMachine<'a> {
        RegisterMachine {
            acc: 0,
            reg: [0; 32],
            instructions: instructions,
            cur: 0,
            flags: map,
            end: false,
        }
    }

    pub fn run(&mut self) {
        while !self.end {
            let i = self.instructions.get(self.cur).unwrap();
            let op = i.operand.as_str();

            match i.instruct.as_str() {
                "print" => self.print(),
                "load" => self.load(op),
                "store" => self.store(op),
                "add" => self.add(op),
                "sub" => self.sub(op),
                "mul" => self.mul(op),
                "div" => self.div(op),
                "end" => self.end(),
                "goto" => self.goto(op),
                "jzero" => self.jzero(op),
                "jnzero" => self.jnzero(op),
                _ => panic!("Unknown instruction {}", i.instruct),
            };

            self.cur += 1;
        }
    }

    fn print(&self) {
        let mut reg = String::new();
        for i in 0..32 {
            reg.push_str(self.reg[i].to_string().as_str());
            reg.push_str(", ");
        }
        reg.truncate(reg.len() - 2);

        println!("ACC: {}", self.acc);
        println!("REG: {}", reg);
    }

    // LOAD
    fn load(&mut self, s: &str) {
        let (n, _) = self.get_val(s);
        self.acc = n;
    }

    // STORE
    fn store(&mut self, s: &str) {
        let (n, _) = self.get_addr(s);
        self.reg[n as usize] = self.acc;
    }

    // ADD
    fn add(&mut self, s: &str) {
        let (n, _) = self.get_val(s);
        self.acc += n;
    }

    // SUB
    fn sub(&mut self, s: &str) {
        let (n, _) = self.get_val(s);
        // 0 on underflow
        match self.acc.checked_sub(n) {
            Some(n) => self.acc = n,
            None => self.acc = 0,
        }
    }

    // MUL
    fn mul(&mut self, s: &str) {
        let (n, _) = self.get_val(s);
        self.acc *= n;
    }

    // DIV
    fn div(&mut self, s: &str) {
        let (n, _) = self.get_val(s);
        self.acc /= n;
    }

    // END
    fn end(&mut self) {
        self.end = true;
    }

    // GOTO
    fn goto(&mut self, s: &str) {
        self.cur = *self.flags.get(s).unwrap() - 1;
    }

    // JZERO
    fn jzero(&mut self, s: &str) {
        if self.acc == 0 {
            self.goto(s);
        }
    }

    // JNZERO
    fn jnzero(&mut self, s: &str) {
        if self.acc != 0 {
            self.goto(s);
        }
    }

    fn get_val(&self, s: &str) -> (u8, OperandType) {
        let arg_type = operand_type(s);

        match arg_type {
            OperandType::Const => (s[1..].parse::<u8>().unwrap(), arg_type),
            OperandType::Ref => (self.reg[s.parse::<u8>().unwrap() as usize], arg_type),
            OperandType::Ptr => {
                let v = self.reg[s[1..].parse::<usize>().unwrap()];
                self.get_val(v.to_string().as_str())
            }
        }
    }

    fn get_addr(&self, s: &str) -> (u8, OperandType) {
        let arg_type = operand_type(s);

        match arg_type {
            OperandType::Const => panic!("Cannot use constant as address"),
            OperandType::Ref => (s.parse::<u8>().unwrap(), arg_type),
            OperandType::Ptr => self.get_val(&s[1..]),
        }
    }
}

#[derive(Clone)]
struct Instruction {
    instruct: String,
    operand: String,
    flag: String,
}

impl Instruction {
    pub fn from(line: &str) -> result::Result<Instruction, CompileError> {
        let args: Vec<&str> = line.split(" ").collect();
        let (mut flag, mut op) = (String::new(), String::new());
        let instruct;

        // FMT: IN OP || M IN <OP>
        if args.len() >= 2 {
            // FMT: M IN <OP>
            let first = args.first().unwrap().to_lowercase();
            if first.starts_with("m") && first != "mul" {
                flag = first;
                instruct = args.get(1).unwrap().to_lowercase();
                // FMT: M IN OP
                if args.len() == 3 {
                    op = args.last().unwrap().to_lowercase();
                }
            // FMT: IN OP
            } else {
                instruct = args.get(0).unwrap().to_lowercase();
                op = args.last().unwrap().to_lowercase();
            }
        // FMT: IN
        } else {
            instruct = args.get(0).unwrap().to_lowercase();
        }

        Ok(Instruction {
            instruct: instruct,
            operand: op,
            flag: flag,
        })
    }
}

fn main() {
    // Parse CLI args
    let mut args: Vec<String> = env::args().collect();
    args.remove(0);

    if args.len() == 0 {
        println!("fatal error: no input files");
        process::exit(1);
    }

    for a in args {
        match read_line(PathBuf::from(a)) {
            Ok(_) => (),
            Err(e) => panic!("Failed to process file: {}", e),
        };
    }
}

fn read_line(path: PathBuf) -> io::Result<()> {
    let file = File::open(path)?;

    let mut reader = BufReader::new(file);
    let mut line = String::new();

    let mut instructs: Vec<Instruction> = Vec::new();
    let mut flags: HashMap<String, usize> = HashMap::new();

    loop {
        match reader.read_line(&mut line) {
            Ok(bytes_read) => {
                // EOF
                if bytes_read == 0 {
                    break;
                }

                // Strip \n (EOL)
                line = line.replace("\n", "");
                line = line.replace("\r", "");
                // Replace \t (TAB)
                line = line.replace("\t", " ");

                if line.len() > 0 || line.starts_with("\n") {
                    match Instruction::from(&line) {
                        Ok(i) => {
                            if !i.flag.is_empty() {
                                if flags.contains_key(&i.flag) {
                                    panic!("Flag {} already defined", i.flag);
                                }
                                flags.insert(i.flag.clone(), instructs.len());
                            }
                            instructs.push(i);
                        }
                        Err(e) => panic!(e),
                    }
                }

                line.clear();
            }
            Err(e) => return Err(e),
        }
    }

    let r = &mut RegisterMachine::new(&instructs, &flags);
    r.run();

    Ok(())
}
